package org.example;

public class Ingresso {
    private String nome, cpf;
    private Evento evento;
    private ECategoriaDeIngressos eCategoriaDeIngressos;



    public Ingresso(String nome, String cpf, Evento evento, ECategoriaDeIngressos eCategoriaDeIngressos) {
        this.nome = nome;
        this.cpf = cpf;
        this.evento = evento;
        this.eCategoriaDeIngressos = eCategoriaDeIngressos;


    }

    public void resumoCompraDeIngressos(Evento ev){
        System.out.println(Evento.SEPARADOR);
        System.out.println("Nome: " + getNome());
        System.out.println("CPF: " + getCpf());
        System.out.println("Nome do evento: " + ev.getNomeEvento());
        System.out.println("Data do evento: " + ev.getData());
        System.out.println("Categoria do ingresso: " + eCategoriaDeIngressos);

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }


    public ECategoriaDeIngressos geteCategoriaDeIngressos() {
        return eCategoriaDeIngressos;
    }

    public void seteCategoriaDeIngressos(ECategoriaDeIngressos eCategoriaDeIngressos) {
        this.eCategoriaDeIngressos = eCategoriaDeIngressos;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    @Override
    public String toString() {
        return
                "Ingresso: " + this.nome + "\n" +
                "Categoria: " + this.eCategoriaDeIngressos;
    }
}

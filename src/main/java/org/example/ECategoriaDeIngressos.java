package org.example;

public enum ECategoriaDeIngressos implements ICategoriaIngresso{

    PISTA{
        @Override
        public double calcularValorIngresso(Evento evento) {
            return evento.getValorUnico();
        }

        @Override
        public void imprimirValorIngresso(Evento evento) {
            System.out.println("Ingresso Categoria Pista");
            System.out.println("Valor: R$" + calcularValorIngresso(evento));
        }

    },

    VIP{
        @Override
        public double calcularValorIngresso(Evento evento) {
            return evento.getValorUnico() + ((evento.getValorUnico() * 30)/100);
        }

        @Override
        public void imprimirValorIngresso(Evento evento) {
            System.out.println("Ingresso Categoria VIP");
            System.out.println("Valor: R$" + calcularValorIngresso(evento));
        }

    },

    Camarote {
        @Override
        public double calcularValorIngresso(Evento evento){
            return evento.getValorUnico() + ((evento.getValorUnico() * 60)/100);
        }

        @Override
        public void imprimirValorIngresso(Evento evento){
            System.out.println("Ingresso Categoria Camarote");
            System.out.println("Valor: R$" + calcularValorIngresso(evento));
        }
    };

}

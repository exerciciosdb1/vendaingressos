package org.example;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        Evento ev = new Evento("Curso de java", LocalDate.now(), 150, 3);

        Ingresso ing = new Ingresso("Verusca", "01535980001", ev, ECategoriaDeIngressos.VIP);
        Ingresso ing1 = new Ingresso("Jujubinha", "02535690002", ev, ECategoriaDeIngressos.Camarote);
        Ingresso ing2 = new Ingresso("Alguém", "02535610123", ev, ECategoriaDeIngressos.PISTA);



        ev.venderIngresso(ing);
        ing.geteCategoriaDeIngressos().imprimirValorIngresso(ev);
        ing.resumoCompraDeIngressos(ev);

        ev.venderIngresso(ing1);
        ing1.geteCategoriaDeIngressos().imprimirValorIngresso(ev);

        ev.venderIngresso(ing2);
        ing2.geteCategoriaDeIngressos().imprimirValorIngresso(ev);

        ev.quantidadeIngVendido();

        ev.listaIngressoVendido();

    }


}
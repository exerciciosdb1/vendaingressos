package org.example;

public interface ICategoriaIngresso {
    double calcularValorIngresso(Evento evento);
    void imprimirValorIngresso(Evento evento);

}

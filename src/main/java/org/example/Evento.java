package org.example;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class Evento{
    private String nomeEvento;
    private LocalDate data;
    private double valorUnico;
    private int capacidadeMaxima;


    ArrayList<Ingresso> listaIngressos = new ArrayList<>();
    public static final String SEPARADOR = "---------//--------";

    public Evento(String nomeEvento, LocalDate data, double valorUnico, int capacidadeMaxima) {
        this.nomeEvento = nomeEvento;
        this.data = data;
        this.valorUnico = valorUnico;
        this.capacidadeMaxima = capacidadeMaxima;
    }
    public void listaIngressoVendido() {
        System.out.println(SEPARADOR);
        System.out.println("Lista de Ingresso Vendido:");
        for (Ingresso i : listaIngressos) {
            System.out.println(i);

        }
    }
    public void venderIngresso(Ingresso ingresso) { //passar categoria do ingresso
        System.out.println(SEPARADOR);
        if (listaIngressos.size() < this.capacidadeMaxima) {
            listaIngressos.add(ingresso);
            System.out.println("Ingresso para o evento " + this.nomeEvento + " vendido");
        } else {
            System.out.println("Exedeu o número máximo de pessoas");
        }
    }

    public void quantidadeIngVendido(){
        System.out.println(SEPARADOR);
        System.out.println("A quantidade de ingressos vendidos ja foram de " + listaIngressos.size());

    }


    public String getNomeEvento() {
        return nomeEvento;
    }

    public void setNomeEvento(String nomeEvento) {
        this.nomeEvento = nomeEvento;
    }

    public double getValorUnico() {
        return valorUnico;
    }

    public void setValorUnico(double valorUnico) {
        this.valorUnico = valorUnico;
    }

    public int getCapacidadeMaxima() {
        return capacidadeMaxima;
    }

    public void setCapacidadeMaxima(int capacidadeMaxima) {
        this.capacidadeMaxima = capacidadeMaxima;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }


}
